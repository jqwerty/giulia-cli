<?php

error_reporting(0);

include "lib.php";

echo "  oooooooooooo    oooooooooo     oooo         oooo     oooo              oooo           oooooooo \n";
echo " oooooooooooo      oooooooo       oo           oo       oo                oo             oo ooo \n";
echo "ooo                   oo          oo           oo       oo                oo            oo   ooo \n";
echo "oo                    oo          oo           oo       oo                oo           oo     ooo \n";
echo "oo     oooooo         oo          oo           oo       oo                oo          oo       ooo \n";
echo "oo     oooooo         oo          ooo         ooo       oo      oooo      oo         oooooooooooooo \n";
echo "ooo       oo          oo           ooo       ooo        oo       oo       oo        oo           ooo \n";
echo " ooooooooooo        ooooooo         oooo   oooo         ooooooooooo       oo       oo             ooo \n";
echo "  ooooooooooo      ooooooooo          ooooooo          ooooooooooooo     oooo     oooo           ooooo \n";
echo "======================================== CLI EDITION BETA =========================================\n";
echo "\n";
echo "Insert a keyword: ";
$keyword = trim(fgets(STDIN));
$key = strtolower(str_replace(" ", "+", $keyword));
$url = "ahmia.fi/search/?q=" . $key;

$read = ahmia("$url");

$dom = new DOMDocument();

@$dom->loadHTML($read);

//SEARCH CLASS
$classname = "searchResult";
$finder = new DomXPath($dom);
$spaner = $finder->query("//*[contains(@class, '$classname')]");

//GRAB DATA FROM CLASS
$span = $spaner->item(0);

$title = $span->getElementsByTagName('h4');
$link = $span->getElementsByTagName('cite');

$number = $no++;

$data = array();
foreach ($title as $val){
  $data[] = array(
    'title' => $title->item($number)->nodeValue,
    'link' => $link->item($number)->nodeValue,
  );
  $number++;
}

$total = count($data);
echo $total . " Result found\n";

$kk = 1;

foreach($data as $val)
{
	echo $kk++ . " " . $val['title'] . "\n";
	echo "\n";
	echo $val['link'] . "\n\n";
}

?>